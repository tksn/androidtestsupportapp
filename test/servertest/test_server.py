import base64
import datetime
import json
import os
import random
import subprocess
import sys
import time
import pytest
import requests


REMOTE_PORT = 5005


def gradle_instrument_arg(name, value):
    return ''.join(('-P', 'android.testInstrumentationRunnerArguments.', name, '=', value))


GRADLE_INSTALL_TASKS = ['installDebug', 'installDebugAndroidTest']
GRADLE_INSTALL_OPTIONS = ['--console=plain']

GRADLE_TEST_TASKS = ['connectedDebugAndroidTest']
GRADLE_TEST_OPTIONS = [
    '--console=plain',
    gradle_instrument_arg('notClass',
                          'org.bitbucket.tksn.androidtestsupportapp.TestSupportMain'),
    gradle_instrument_arg('port', str(REMOTE_PORT))
]

if sys.platform == 'win32':
    GRADLEW = 'gradlew.bat'
else:
    GRADLEW = './gradlew'

MAINPACKAGE = 'org.bitbucket.tksn.androidtestsupportapp'
TESTPACKAGE = 'org.bitbucket.tksn.androidtestsupportapp.test'
TESTRUNNER = 'android.support.test.runner.AndroidJUnitRunner'

FORWARD_REMOTE_PORT = REMOTE_PORT
FORWARD_LOCAL_PORT = 5995
SERVERURL = 'http://localhost:{}'.format(FORWARD_LOCAL_PORT)
ENDPOINT_JSONRPC = SERVERURL + '/jsonrpc/0'


def perr(s):
    print(s, file=sys.stderr)


def start_testrunner(run='main'):
    stop_testrunner()
    class_specifier = 'class' if run == 'main' else 'notClass'
    proc = subprocess.Popen(
        ['adb', 'shell', 'am', 'instrument', '-r', '-w',
         '-e', class_specifier, 'org.bitbucket.tksn.androidtestsupportapp.TestSupportMain',
         '-e', 'port', str(FORWARD_REMOTE_PORT),
        '/'.join((TESTPACKAGE, TESTRUNNER))])
    if run != 'main':
        proc.communicate()
    return proc


def stop_testrunner(runnerproc=None):
    if runnerproc is not None:
        runnerproc.kill()
        runnerproc.communicate()
    subprocess.run(['adb', 'shell', 'am', 'force-stop', MAINPACKAGE])
    subprocess.run(['adb', 'shell', 'am', 'force-stop', TESTPACKAGE])


def start_forwarding():
    localport = 'tcp:{}'.format(FORWARD_LOCAL_PORT)
    remoteport = 'tcp:{}'.format(FORWARD_REMOTE_PORT)
    proc = subprocess.run(
        ['adb', 'forward', localport, remoteport])
    return proc.returncode


def stop_forwarding():
    localport = 'tcp:{}'.format(FORWARD_LOCAL_PORT)
    proc = subprocess.run(['adb', 'forward', '--remove', localport])


def jsonrpc_request(method, params=None):
    params = params or []
    reqid = random.randrange(0, 2^32)
    headers = {'content-type': 'application/json'}
    payload = {
        'method': method,
        'params': params,
        'jsonrpc': '2.0',
        'id': reqid
    }
    response = requests.post(
        ENDPOINT_JSONRPC, data=json.dumps(payload), headers=headers).json()
    return response


def wait_for_server(wait_until_die=False):
    PING_COUNT = 20
    PING_INTERVAL = 5.0  # sec

    def ping():
        try:
            pong = jsonrpc_request('ping')
        except requests.RequestException:
            return None
        return pong

    for i in range(PING_COUNT):
        pong = ping()
        if pong is not None:
            if pong.get('result') != 'pong':
                raise RuntimeError('Test server response error')
        if wait_until_die and pong is None:
            return
        if not wait_until_die and pong is not None:
            return
        time.sleep(PING_INTERVAL)

    raise RuntimeError('Test server timeout')


def circleci_save_screenshot():
    outdir = os.environ.get('CIRCLE_ARTIFACTS')
    if not outdir:
        return
    dt = datetime.datetime.now()
    filename = 'screen_{}.png'.format(dt.strftime('%Y%m%d%H%M%S'))
    outpath = os.path.join(outdir, filename)
    subprocess.run(['adb', 'shell', 'screencap', '-p', '/sdcard/screen.png'])
    subprocess.run(['adb', 'pull', '/sdcard/screen.png', outpath])


@pytest.fixture(scope='session', autouse=True)
def android_testserver(request):
    scriptdir = os.path.dirname(os.path.abspath(__file__))
    projectdir = os.path.normpath(os.path.join(scriptdir, '..', '..'))
    os.chdir(projectdir)
    perr('==BUILD/INSTALL===============')
    perr('--GRADLE_TASKS_START--------')
    gradle_command = [GRADLEW] + GRADLE_INSTALL_TASKS + GRADLE_INSTALL_OPTIONS
    perr('command = {}'.format(gradle_command))
    proc = subprocess.run(gradle_command)
    perr('--GRADLE_TASKS_FINISH ({})---'.format(proc.returncode))
    if proc.returncode != 0:
        raise RuntimeError('Gradle task(s) failed')
    perr('==TEST(FromOutsideOfDevice)=====')
    perr('--INSTRUMENT_START----------')
    runner_proc = start_testrunner()
    perr('--FORWARDING_START----------')
    if start_forwarding() != 0:
        raise RuntimeError('Could not start tcp forwarding')
    perr('--WAITING FOR TESTSERVER----')
    wait_for_server()
    perr('--TEST_START----------------')
    def fin():
        perr('')
        terminate_testserver()
        perr('--TESTSERVER_STOP----------')
        stop_forwarding()
        perr('--FORWARDING_STOP----------')
        stop_testrunner(runner_proc)
        perr('--INSTRUMENT_STOP----------')

        perr('==TEST(InDevice)=============')
        perr('--GRADLE_TASKS_START--------')
        gradle_command = [GRADLEW] + GRADLE_TEST_TASKS + GRADLE_TEST_OPTIONS
        perr('command = {}'.format(gradle_command))
        circleci_save_screenshot()
        proc = subprocess.run(gradle_command)
        circleci_save_screenshot()
        perr('--GRADLE_TASKS_FINISH ({})---'.format(proc.returncode))
        if proc.returncode != 0:
            raise RuntimeError('Gradle task(s) failed')
        perr('==COMPLETED==================')
    request.addfinalizer(fin)


TESTDATADIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'example_apk')
TESTDATAPACKAGE = 'org.bitbucket.tksn.example'


def read_apk(filename):
    apk_path = os.path.join(TESTDATADIR, filename)
    with open(apk_path, 'rb') as f:
        return base64.b64encode(f.read()).decode('utf-8')


def full(classname):
    return TESTDATAPACKAGE + '.' + classname


def request_run(params):
    p = [params[field] for field in ('apkData', 'className')]
    response = jsonrpc_request('run', p)
    return response


def assert_result(expected, response):
    #print(response)
    if expected == 'error':
        assert 'error' in response
    else:
        assert 'result' in response


def terminate_testserver():
    assert_result(
        'success',
        jsonrpc_request('stopServer'))
    wait_for_server(wait_until_die=True)


def test_error_against_invalidapkdata():
    assert_result('error', request_run({
        'apkData': 'invalidapkdatainvalidapkdata',
        'className': full('Minimal')
        }))


def test_error_against_invalidclassname():
    assert_result('error', request_run({
        'apkData': read_apk('minimal_valid.apk'),
        'className': 'invalidclassname'
        }))


def test_able_to_run_minimal_script():
    assert_result('success', request_run({
        'apkData': read_apk('minimal_valid.apk'),
        'className': full('Minimal')
        }))


def test_able_to_run_scriptwithtestlibs():
    assert_result('success', request_run({
        'apkData': read_apk('minimal_with_testlibs_valid.apk'),
        'className': full('MinimalWithTestLibs')
        }))


def test_unable_to_run_class_without_callable_interface():
    assert_result('error', request_run({
        'apkData': read_apk('notcallable_invalid.apk'),
        'className': full('NotCallable')
        }))


def test_able_to_run_multiclass():
    assert_result('success', request_run({
        'apkData': read_apk('multiclass.apk'),
        'className': full('Class0')
        }))


def test_able_to_run_classwiththirdpartylib():
    assert_result('success', request_run({
        'apkData': read_apk('withthirdpartylib.apk'),
        'className': full('WithThirdPartyLib')
        }))

