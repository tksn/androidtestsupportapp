package org.bitbucket.tksn.androidtestsupportapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.Enumeration;


public class MainActivity extends AppCompatActivity
        implements ImageReader.OnImageAvailableListener {

    public static final String ACTION_START_SCREENCAP =
            "org.bitbucket.tksn.androidtestsupportapp.START_SCREENCAP";
    public static final String ACTION_STOP_SCREENCAP =
            "org.bitbucket.tksn.androidtestsupportapp.STOP_SCREENCAP";
    private static final int PROJECTION_REQUEST_CODE = 100;

    BroadcastReceiver mReceiver = null;
    static MediaProjectionManager sMediaProjectionManager = null;
    private MediaProjection mMediaProjection = null;
    private VirtualDisplay mVirtualDisplay = null;
    private ImageReader mImageReader = null;
    private Point mScreenSize = new Point();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String addr = getIpv4Address();
        if (addr != null) {
            TextView view = (TextView)findViewById(R.id.ipv4Text);
            if (view != null) view.setText("IPv4 Address: " + addr);
        }

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ACTION_START_SCREENCAP)) {
                    startScreenCapture();
                } else {
                    stopScreenCapture();
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_START_SCREENCAP);
        intentFilter.addAction(ACTION_STOP_SCREENCAP);
        //registerReceiver(receiver, intentFilter);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    private void startScreenCapture() {
        sMediaProjectionManager =
                (MediaProjectionManager)getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        startActivityForResult(
                sMediaProjectionManager.createScreenCaptureIntent(), PROJECTION_REQUEST_CODE);
    }

    private void stopScreenCapture() {
        if (mMediaProjection != null) {
            mMediaProjection.stop();
            mMediaProjection = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PROJECTION_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            mMediaProjection = sMediaProjectionManager.getMediaProjection(resultCode, data);
            mMediaProjection.registerCallback(new MediaProjectionCallback(), null);

            getWindowManager().getDefaultDisplay().getRealSize(mScreenSize);
            mImageReader = ImageReader.newInstance(
                    mScreenSize.x, mScreenSize.y, PixelFormat.RGBA_8888, 2);
            mVirtualDisplay = mMediaProjection.createVirtualDisplay(
                    "screen-mirror",
                    mScreenSize.x, mScreenSize.y,
                    getResources().getDisplayMetrics().densityDpi,
                    DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                    mImageReader.getSurface(), null, null);
            mImageReader.setOnImageAvailableListener(this, null);
        }
    }

    @Override
    public void onImageAvailable(ImageReader reader) {
        Image image = reader.acquireLatestImage();
        if (image == null) return;

        Image.Plane[] planes = image.getPlanes();
        ByteBuffer buffer = planes[0].getBuffer();
        int rowStride = planes[0].getRowStride();
        int pixelStride = planes[0].getPixelStride();
        int rowStrideInPixels = rowStride / pixelStride;
        Bitmap bitmap = Bitmap.createBitmap(
                rowStrideInPixels, mScreenSize.y, Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(buffer);
        image.close();

        TestSupportApplication app = (TestSupportApplication)getApplication();
        app.setScreenshot(Bitmap.createBitmap(bitmap, 0, 0, mScreenSize.x, mScreenSize.y));
        updateMediaProjectionStateText(true);
    }

    class MediaProjectionCallback extends MediaProjection.Callback {
        @Override
        public void onStop() {
            mMediaProjection = null;
            if (mVirtualDisplay != null) {
                mVirtualDisplay.release();
                mVirtualDisplay = null;
            }
            if (mImageReader != null) {
                mImageReader.close();
                mImageReader = null;
            }
            TestSupportApplication app = (TestSupportApplication)getApplication();
            app.setScreenshot(null);
            updateMediaProjectionStateText(false);
        }
    }

    private static String getIpv4Address() {
        try {
            Enumeration<NetworkInterface> interfEnum = NetworkInterface.getNetworkInterfaces();
            while(interfEnum.hasMoreElements()) {
                NetworkInterface interf = interfEnum.nextElement();
                Enumeration<InetAddress> addrs = interf.getInetAddresses();

                while(addrs.hasMoreElements()) {
                    InetAddress addr = addrs.nextElement();
                    if(!addr.isLoopbackAddress() && addr instanceof Inet4Address) {
                        return addr.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void updateMediaProjectionStateText(boolean isRunning) {
        String text = isRunning ? "MediaProjection: RUNNING" : "MediaProjection: IDLE";
        TextView view = (TextView)findViewById(R.id.mediaProjectionStateText);
        if (view != null) {
            if (!view.getText().equals(text)) {
                view.setText(text);
            }
        }
    }
}
