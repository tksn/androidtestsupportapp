package org.bitbucket.tksn.androidtestsupportapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import java.io.ByteArrayOutputStream;

/**
 * Created by tksn on 2016/08/16.
 */
public class TestSupportRpcServiceImpl implements TestSupportRpcService {

    boolean mStopRequested;
    Context mContext;

    public TestSupportRpcServiceImpl(Context context) {
        mStopRequested = false;
        mContext = context;
    }

    public boolean stopRequested() {
        return mStopRequested;
    }

    public void setActivityAsContext(Activity activity) {
        mContext = activity;
    }

    @Override
    public void stopServer() {
        mStopRequested = true;
    }

    @Override
    public boolean isScreenshotReady() {
        TestSupportApplication app = getApplication();
        return app != null && app.getScreenshot() != null;
    }

    @Override
    public String getVersionName() {
        PackageManager pm = mContext.getPackageManager();
        PackageInfo info = null;
        try {
            info = pm.getPackageInfo(mContext.getPackageName(), 0);
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return info == null ? "" : info.versionName;
    }

    @Override
    public byte[] getScreenshot(int width, int height, int quality) {
        TestSupportApplication app = getApplication();
        if (app == null) return null;
        Bitmap screenshot = app.getScreenshot();
        if (screenshot == null) return null;
        if (width > 0 && height > 0) {
            screenshot = Bitmap.createScaledBitmap(screenshot, width, height, false);
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        screenshot.compress(Bitmap.CompressFormat.PNG, quality, outputStream);
        return outputStream.toByteArray();
    }

    @Override
    public void startScreenCapturing() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.ACTION_START_SCREENCAP);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

    @Override
    public void stopScreenCapturing() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.ACTION_STOP_SCREENCAP);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

    @Override
    public void run(String apkData, String className) throws DexRunnerService.UnableToRunException {
        DexRunnerService.Action.runApk(mContext, apkData, className);
    }

    @Nullable
    private TestSupportApplication getApplication() {
        if (mContext == null) return null;
        Activity activity = (Activity)mContext;
        return (TestSupportApplication)activity.getApplication();
    }
}
