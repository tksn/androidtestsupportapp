package org.bitbucket.tksn.androidtestsupportapp;

import com.googlecode.jsonrpc4j.JsonRpcError;
import com.googlecode.jsonrpc4j.JsonRpcErrors;

/**
 * Created by tksn on 2016/08/16.
 */
public interface TestSupportRpcService {
    static final int ERROR_CODE_BASE = -32000;

    void stopServer();

    boolean isScreenshotReady();

    String getVersionName();

    byte[] getScreenshot(int width, int height, int quality);

    void startScreenCapturing();

    void stopScreenCapturing();

    @JsonRpcErrors({@JsonRpcError(exception=DexRunnerService.UnableToRunException.class, code=ERROR_CODE_BASE-10)})
    void run(String apkData, String className) throws DexRunnerService.UnableToRunException;
}
