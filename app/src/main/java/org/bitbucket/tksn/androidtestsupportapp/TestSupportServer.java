package org.bitbucket.tksn.androidtestsupportapp;

import android.app.Activity;
import android.content.Context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.uiautomator.stub.AutomatorService;
import com.github.uiautomator.stub.AutomatorServiceImpl;
import com.googlecode.jsonrpc4j.JsonRpcServer;
import com.googlecode.jsonrpc4j.ProxyUtil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

/**
 * Created by tksn on 2016/08/16.
 */
public class TestSupportServer extends NanoHTTPD {

    private static final String MIMETYPE_TEXT = "text/plain";
    private static final String MIMETYPE_JSONRPC = "application/json";
    private static final String MIMETYPE_PNG = "image/png";

    private JsonRpcServer mJsonRpcServer;
    private TestSupportRpcServiceImpl mTestSupportService;

    public TestSupportServer(int port, Context context) {
        super(port);

        mTestSupportService = new TestSupportRpcServiceImpl(context);
        Object compositeService = ProxyUtil.createCompositeServiceProxy(
                this.getClass().getClassLoader(),
                new Object[] { mTestSupportService, new AutomatorServiceImpl() },
                new Class<?>[] { TestSupportRpcService.class, AutomatorService.class },
                true);
        mJsonRpcServer = new JsonRpcServer(new ObjectMapper(), compositeService);
    }

    public boolean stopRequested() {
        return mTestSupportService.stopRequested();
    }

    public void setActivityAsContext(Activity activity) {
        mTestSupportService.setActivityAsContext(activity);
    }

    @Override
    public Response serve(IHTTPSession session) {
        if (session.getUri().startsWith("/screenshot.png")) {
            return handleScreenshotRequest(session);
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            mJsonRpcServer.handleRequest(session.getInputStream(), outputStream);
            outputStream.flush();
        }
        catch (IOException ex) {
            return newFixedLengthResponse(
                    Response.Status.INTERNAL_ERROR, MIMETYPE_TEXT, ex.getMessage());
        }

        byte[] outputBytes = outputStream.toByteArray();
        ByteArrayInputStream data = new ByteArrayInputStream(outputBytes);
        return newFixedLengthResponse(
                Response.Status.OK, MIMETYPE_JSONRPC, data, outputBytes.length);
    }

    private Response handleScreenshotRequest(IHTTPSession session) {
        Map<String, List<String>> params = session.getParameters();
        int width = getIntParam(params, "width", 0);
        int height = getIntParam(params, "height", 0);
        int quality = getIntParam(params, "quality", 90);
        byte[] outputBytes = mTestSupportService.getScreenshot(width, height, quality);
        if (outputBytes == null) {
            return newFixedLengthResponse(
                    Response.Status.NOT_FOUND, MIMETYPE_TEXT, "Image not available");
        }
        ByteArrayInputStream data = new ByteArrayInputStream(outputBytes);
        return newFixedLengthResponse(
                Response.Status.OK, MIMETYPE_PNG, data, outputBytes.length);
    }

    private static int getIntParam(Map<String, List<String>> params, String name, int defaultValue) {
        if (params.containsKey(name)) {
            List<String> l = params.get(name);
            if (l.size() > 0) return Integer.parseInt(l.get(0));
        }
        return defaultValue;
    }
}
