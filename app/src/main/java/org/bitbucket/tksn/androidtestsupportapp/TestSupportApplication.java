package org.bitbucket.tksn.androidtestsupportapp;

import android.app.Application;
import android.graphics.Bitmap;

/**
 * Created by tksn on 2016/08/17.
 */
public class TestSupportApplication extends Application {

    Bitmap mLatestScreenshot = null;

    public void setScreenshot(Bitmap screenshot) {
        mLatestScreenshot = screenshot;
    }

    public Bitmap getScreenshot() {
        return mLatestScreenshot;
    }

}
