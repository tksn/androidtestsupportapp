package org.bitbucket.tksn.androidtestsupportapp


import android.app.IntentService
import android.app.Notification
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.util.Log

import android.support.v7.app.NotificationCompat
import java.io.File

/**
 * On-the-fly Callable load&run service
 */
class DexRunnerService : IntentService {
    private val LOGTAG: String = "DexRunnerService"
    private val ACTION_RUN: String = "org.bitbucket.tksn.androidtestsupportapp.RUN"
    private val RUN_PARAM_APKFILE: String = "RUN_PARAM_APKFILE"
    private val RUN_PARAM_CLASSNAME: String = "RUN_PARAM_CLASSNAME"
    private val NOTIFICATION_ID_START = 1
    private val NOTIFICATION_ID_STOP = 2

    constructor() : super("DexRunnerService") {
    }

    /**
     * Dispatches action handler
     */
    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            when (intent.action) {
                ACTION_RUN -> handleActionRun(
                        intent.getStringExtra(RUN_PARAM_APKFILE),
                        intent.getStringExtra(RUN_PARAM_CLASSNAME))
            }
        }
    }

    companion object Action {
        /**
         * queues RUN action
         *
         * @param context Context object
         * @param apkData Base64 encoded bytes of the APK file
         * @param className name of the class to run
         */
        fun runApk(context: Context, apkData: String, className: String) {
            try {
                val apkFile = decodeAndSaveAsApk(apkData, context.cacheDir)

                // Create runner just to see if the class can be successfully loaded
                DexRunner().checkRunnable(apkFile, className, context.cacheDir, context.classLoader)

                val runnerService = DexRunnerService()
                val intent = Intent(context, runnerService.javaClass)
                intent.action = runnerService.ACTION_RUN
                intent.putExtra(runnerService.RUN_PARAM_APKFILE, apkFile.absolutePath)
                intent.putExtra(runnerService.RUN_PARAM_CLASSNAME, className)
                context.startService(intent)
            } catch (e: Throwable) {
                throw UnableToRunException("Exception thrown", e);
            }
        }
    }

    /**
     * Handles RUN action
     *
     * @param apkFilePath file path to the APK file
     * @param className the class name to load & run
     */
    private fun handleActionRun(apkFilePath: String, className: String) {
        showStartNotification(className)
        val apkFile = File(apkFilePath)
        try {
            DexRunner().run(apkFile, className, cacheDir, classLoader)
            showStopNotification(className, "Completed")
        } catch (e: Throwable) {
            e.printStackTrace()
            Log.i(LOGTAG, "exception occurred while running")
            showStopNotification(className, "Error occurred")
        }
    }

    /**
     * Displays start notification
     */
    private fun showStartNotification(className: String) {
        showNotification(
                NOTIFICATION_ID_START,
                "Test started",
                "Class: " + className,
                "")
    }

    /**
     * Displays stop notification
     */
    private fun showStopNotification(className: String, statusString: String) {
        showNotification(
                NOTIFICATION_ID_STOP,
                "Test stopped",
                "Reason: " + statusString,
                "Class: " + className)
    }

    /**
     * Displays notification
     */
    private fun showNotification(id: Int, title: String, text: String, subText: String) {
        val builder = NotificationCompat.Builder(this)
        builder.setSmallIcon(R.mipmap.ic_launcher)
        builder.setContentTitle(title)
        builder.setContentText(text)
        builder.setSubText(subText)
        builder.setAutoCancel(true)
        builder.setPriority(Notification.PRIORITY_HIGH)
        builder.setVibrate(longArrayOf(0, 500, 500, 500, 500))
        val manager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        manager.notify(id, builder.build())
    }

    class UnableToRunException(message: String, cause: Throwable) :
            Exception(message + ": " + cause.message, cause)
}

