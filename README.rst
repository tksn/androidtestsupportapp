AndroidTestSupportApp
=========================

Purpose
-------

This software is a HTTP server which runs on an android device (API level >= 21),
and it provides following functions:

- JSON-RPC interface to uiautomator_
- Serves resized screenshot
- On-the-fly APK loading/running

.. _uiautomator: https://developer.android.com/topic/libraries/testing-support-library/index.html


Status
------

Pre-alpha


Notes
-----

This software is based on android-uiautomator-server_ .
,and depends some other software such as jackson_, jsonrpc4j_ and nanohttpd_ .

.. _android-uiautomator-server: https://github.com/xiaocong/android-uiautomator-server
.. _jackson: https://github.com/FasterXML/jackson
.. _jsonrpc4j: https://github.com/briandilley/jsonrpc4j
.. _nanohttpd: https://github.com/NanoHttpd/nanohttpd

License
-------

See LICENSE.txt

