package org.bitbucket.tksn.androidtestsupportapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.view.WindowManager;

import com.github.uiautomator.stub.AutomatorService;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.action.ViewActions.click;
import static org.junit.Assert.fail;


import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by tksn on 2016/08/16.
 */
public class TestSupportServerTest {

    static final int defaultPort = 9001;
    static final String HOSTURL = "http://localhost";

    TestSupportServer mServer;
    String mServerUrl = null;
    JsonRpcHttpClient mRpcClient;
    TestSupportRpcService mTestSupportService;
    AutomatorService mAutomatorService;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<MainActivity>(MainActivity.class, false, false);

    @Before
    public void setUp() {
        int port = defaultPort;
        Bundle args = InstrumentationRegistry.getArguments();
        if (args.containsKey("port")) {
            String portStr = args.getString("port");
            port = Integer.parseInt(portStr);
        }
        mServerUrl = HOSTURL + ":" + Integer.toString(port);

        mServer = new TestSupportServer(port, InstrumentationRegistry.getTargetContext());
        try { mServer.start(); } catch (IOException ex) {}

        try {
            mRpcClient = new JsonRpcHttpClient(new URL(mServerUrl + "/jsonrpc/0"));
            mTestSupportService = ProxyUtil.createClientProxy(
                    getClass().getClassLoader(), TestSupportRpcService.class, mRpcClient);
            mAutomatorService = ProxyUtil.createClientProxy(
                    getClass().getClassLoader(), AutomatorService.class, mRpcClient);
        }
        catch (MalformedURLException e) {}
    }

    @After
    public void tearDown() {
        mAutomatorService = null;
        mTestSupportService = null;
        mRpcClient = null;
        mServer.stop();
        mServer = null;
    }

    private void launchActivity() {
        final MainActivity activity = mActivityRule.launchActivity(null);
        mServer.setActivityAsContext(activity);

        Runnable wakeUpDevice = new Runnable() {
            @Override
            public void run() {
                activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        };
        activity.runOnUiThread(wakeUpDevice);
    }

    @Test
    public void serverShouldBeStoppedUponRequest() {
        assertFalse(mServer.stopRequested());
        mTestSupportService.stopServer();
        assertTrue(mServer.stopRequested());
    }

    @Test
    public void shouldServeScreenshotState() {
        launchActivity();

        assertFalse(mTestSupportService.isScreenshotReady());

        startScreenCapturing();
        assertTrue(mTestSupportService.isScreenshotReady());
        stopScreenCapturing();
    }

    @Test
    public void shouldServeVersionName() {
        String versionName = mTestSupportService.getVersionName();
        assertTrue(versionName.length() > 0);
    }

    @Test
    public void shouldServeScreenshotViaJsonRpc() {
        launchActivity();

        byte[] bytes = mTestSupportService.getScreenshot(0, 0, 90);
        assertNull(bytes);

        startScreenCapturing();
        bytes = mTestSupportService.getScreenshot(0, 0, 90);
        Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        assertTrue(bmp.getWidth() > 0);
        stopScreenCapturing();
    }

    private Response sendScreenshotRequest() throws IOException {
        Request req = new Request.Builder()
                .url(mServerUrl + "/screenshot.png")
                .get()
                .build();
        OkHttpClient client = new OkHttpClient();
        return client.newCall(req).execute();
    }

    @Test
    public void shouldServeScreenshot() throws IOException {
        launchActivity();

        Response res = sendScreenshotRequest();
        assertEquals(res.code(), 404);
        assertTrue(res.body().string().length() > 0);

        startScreenCapturing();
        res = sendScreenshotRequest();
        assertEquals(res.code(), 200);
        Bitmap bmp = BitmapFactory.decodeStream(res.body().byteStream());
        assertTrue(bmp.getWidth() > 0);
        stopScreenCapturing();
    }

    @Test
    public void shouldAnswerToPing() throws IOException {
        String response = mAutomatorService.ping();
        assertEquals("pong", response);
    }

    static int WAITFOREXISTS_TIMEOUT = 100000;

    private void startScreenCapturing() {
        mTestSupportService.startScreenCapturing();

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        try {
            UiObject startNowButton = device.findObject(new UiSelector().text("Start now"));
            if (!startNowButton.waitForExists(WAITFOREXISTS_TIMEOUT)) {
                fail("MediaProjection START NOW button was not displayed");
            }
            startNowButton.clickAndWaitForNewWindow();

            UiObject runningText = device.findObject(new UiSelector().textContains("RUNNING"));
            if (!runningText.waitForExists(WAITFOREXISTS_TIMEOUT)) {
                fail("Could not run MediaProjection");
            }
        }
        catch (UiObjectNotFoundException e) {}
    }

    private void stopScreenCapturing() {
        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        mTestSupportService.stopScreenCapturing();

        UiObject runningText = device.findObject(new UiSelector().textContains("IDLE"));
        if (!runningText.waitForExists(WAITFOREXISTS_TIMEOUT)) {
            fail("Could not stop MediaProjection");
        }
    }
}
