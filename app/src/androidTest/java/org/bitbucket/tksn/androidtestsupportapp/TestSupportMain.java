package org.bitbucket.tksn.androidtestsupportapp;

import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;


public class TestSupportMain {

    private static final int DEFAULT_PORT = 9001;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<MainActivity>(MainActivity.class);

    @LargeTest
    @Test
    public void run() {
        int port = DEFAULT_PORT;
        Bundle args = InstrumentationRegistry.getArguments();
        if (args.containsKey("port")) {
            String portStr = args.getString("port");
            port = Integer.parseInt(portStr);
        }

        TestSupportServer server = new TestSupportServer(port, mActivityRule.getActivity());

        try {
            server.start();
        }
        catch (IOException e) {
            e.printStackTrace();
            return;
        }

        try {
            while (!server.stopRequested()) {
                Thread.sleep(500);
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        finally {
            server.stop();
        }
    }
}
