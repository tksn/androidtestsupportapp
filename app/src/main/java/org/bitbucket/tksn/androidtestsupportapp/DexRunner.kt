package org.bitbucket.tksn.androidtestsupportapp

import dalvik.system.DexClassLoader
import java.io.File
import java.util.concurrent.Callable

/**
 * On-the-fly Callable loader & runner
 *
 * Loads Callable class from APK file, creates an instance of the class, and runs it.
 */
class DexRunner {

    /**
     * From the given apkFile, loads a Callable class specified by className,
     * creates an instance of the Callable, and runs it.
     *
     * @param apkFile APK file object from which the class is loaded
     * @param className name of the class to load
     * @param optimizedDir directory which is used while loading the APK
     * @param parentLoader parent ClassLoader instance
     */
    fun run(apkFile: File, className: String, optimizedDir: File, parentLoader: ClassLoader) {
        val instance = checkRunnable(apkFile, className, optimizedDir, parentLoader)
        instance.call()
    }

    /**
     * Checks if the class is loadable from the specified apkFile,
     * and also checks if it is Callable.
     * These are done by actually loading the class from the APK and attempting
     * to create a Callable instance using the class.
     *
     * @param apkFile APK file object from which the class is loaded
     * @param className name of the class to load
     * @param optimizedDir directory which is used while loading the APK
     * @param parentLoader parent ClassLoader instance
     * @return Callable instance loaded from
     * @throws ClassNotFoundException
     * @throws ClassCastException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ExceptionInInitializerError
     * @throws SecurityException
     */
    fun checkRunnable(apkFile: File, className: String, optimizedDir: File, parentLoader: ClassLoader) : Callable<*> {
        val loader = DexClassLoader(
                apkFile.absolutePath, optimizedDir.absolutePath, null, parentLoader)
        val classObj = loader.loadClass(className)
        val instance = classObj!!.newInstance()
        if (instance !is Callable<*>)
            throw ClassCastException("$className is not Callable")
        return instance
    }
}
